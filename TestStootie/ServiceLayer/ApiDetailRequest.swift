//
//  DetailApiRequest.swift
//  TestStootie
//
//  Created by john kricorian on 25/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation

struct ApiDetailRequest: ApiRequest {
    
    let stootId: String
    
    var urlRequest: URLRequest {
        
        let url: URL! = URL(string: "https://bff-mobile-dev.stootie.com/demands/\(stootId)/details")
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(RequestParameters.shared.apiVersion, forHTTPHeaderField: RequestParameters.shared.firstHTTPHeaderField)
        request.addValue(RequestParameters.shared.UUID, forHTTPHeaderField: RequestParameters.shared.secondHTTPHeaderField)
        
        return request
    }
}
