//
//  RequestParameters.swift
//  TestStootie
//
//  Created by john kricorian on 25/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation

class RequestParameters {
    
    static var shared = RequestParameters()
    
    private init() {}
    
    let UUID = "057BC3BD-46E1-4125-9F3B-23805CA3132F"
    let firstHTTPHeaderField = "Accept-Version"
    let secondHTTPHeaderField = "X-Request-Id"
    let apiVersion = "3.0.0"
}
