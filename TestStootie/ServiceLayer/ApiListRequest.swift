//
//  StootApiRequest.swift
//  TestStootie
//
//  Created by john kricorian on 25/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation

protocol ApiRequest {
    var urlRequest: URLRequest { get }
}


struct ApiListRequest: ApiRequest {
    
    var urlRequest: URLRequest {
        
        let url = URL(string: "https://bff-mobile-dev.stootie.com/demands/filter")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue(RequestParameters.shared.apiVersion, forHTTPHeaderField: RequestParameters.shared.firstHTTPHeaderField)
        request.addValue(RequestParameters.shared.UUID, forHTTPHeaderField: RequestParameters.shared.secondHTTPHeaderField)
        
        return request
    }
    
}
