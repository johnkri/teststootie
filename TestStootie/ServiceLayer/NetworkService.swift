//
//  NetworkService.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation

typealias DisplayDetailUseCaseCompletionHandler = (Result<Detail?, Error>) -> Void
typealias DisplayListUseCaseCompletionHandler = (Result<List?, Error>) -> Void

protocol NetworkServiceDelegate: class {
    func getList(completion: @escaping DisplayListUseCaseCompletionHandler)
    func getDetail(stootId: String, completion: @escaping DisplayDetailUseCaseCompletionHandler)
}

class NetworkService: NetworkServiceDelegate {
        
    let apiClient: ApiClient
        
    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }
    
    func getList(completion: @escaping (Result<List?, Error>) -> Void) {
 
        let apiListRequest = ApiListRequest()
        apiClient.execute(request: apiListRequest) { (result: Result<ApiResponse<List>, Error>) in
            switch result {
            case let .success(response):
                let list = response.entity
                completion(.success(list))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    func getDetail(stootId: String, completion: @escaping (Result<Detail?, Error>) -> Void) {
        
        let apiDetailRequest = ApiDetailRequest(stootId: stootId)
        
        apiClient.execute(request: apiDetailRequest) { (result: Result<ApiResponse<Detail>, Error>) in
            switch result {
            case let .success(response):
                let detail = response.entity
                completion(.success(detail))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
