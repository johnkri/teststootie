//
//  Result.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation

struct List : Decodable {
    
    let collection: [Stoot]
}

struct Stoot : Decodable {
    
    let user: User
    let package: Package
    let id: Int
    let lat: String
    let lng: String
    let address: String
    let city: String?
    let creationDate: String
    let unitPrice: String
    
    enum CodingKeys : String, CodingKey {
        case user
        case id
        case lat
        case lng
        case address
        case city
        case creationDate = "created_at"
        case package
        case unitPrice = "unit_price"
    }
    
    struct User : Decodable {
        let firstname: String
        let name: String
        let profilePictureUrl: String?
        
        enum CodingKeys : String, CodingKey {
            case firstname
            case name
            case profilePictureUrl = "profile_picture_url"
        }
    }
    
    struct Package : Decodable {
        let name: String
    }
}
