//
//  MainPresenter.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation
import CoreLocation

protocol MainViewPresenterProtocol: class {
    init(view: MainViewProtocol, networkService: NetworkServiceDelegate)
    func getList()
    var list: List? { get set }
    func configure(cell: UserCellViewProtocol, stoot: Stoot)
}

class MainViewPresenter: MainViewPresenterProtocol {
    
    var list: List?
    weak var view: MainViewProtocol?
    let networkService: NetworkServiceDelegate!
    
    required init(view: MainViewProtocol, networkService: NetworkServiceDelegate) {
        self.view = view
        self.networkService = networkService
        getList()
    }
    
    func getList() {
        networkService.getList { [weak self] list in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch list {
                case .success(let list):
                    self.list = list
                    self.view?.success()
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
    
    func configure(cell: UserCellViewProtocol, stoot: Stoot) {
        cell.displayFirstname(firstname: stoot.user.firstname)
        cell.displayCity(city: stoot.city)
        cell.displayAddress(address: "\(displayDistance(lat: stoot.lat, long: stoot.lng))\(" - ")\(displayCreationDate(creationDate: stoot.creationDate))")
        cell.displayPackage(package: stoot.package.name)
        cell.displayUnitPrice(unitPrice: stoot.unitPrice)
        cell.displayImage(imageUrl: stoot.user.profilePictureUrl)
    }
    
    // MARK: - Private
    
    fileprivate func displayCreationDate(creationDate: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "fr_FR_POSIX")
        
        let date = dateFormatter.date(from:creationDate)!
        
        let interval = Calendar.current.dateComponents([.year, .month, .day], from: date, to: Date())
        
        let localizedAgo = NSLocalizedString("ilya", comment: "")
        let localizedYear = NSLocalizedString("Year", comment: "")
        let localizedYears = NSLocalizedString("Years", comment: "")
        let localizedMonth = NSLocalizedString("Month", comment: "")
        let localizedDays = NSLocalizedString("Days", comment: "")
        let localizedDay = NSLocalizedString("Day", comment: "")
        let localizedFromNow = NSLocalizedString("FromNow", comment: "")
        
        if let year = interval.year, year > 0 {
            return year == 1 ? localizedAgo + "\(year)" + " " + localizedYear : localizedAgo +
                "\(year)" + " " + localizedYears
        } else if let month = interval.month, month > 0 {
            return month == 1 ? localizedAgo +  "\(month)" + " " + localizedMonth : localizedAgo +
                "\(month)" + " " + localizedMonth
        } else if let day = interval.day, day > 0 {
            return day == 1 ? localizedAgo +  "\(day)" + " " + localizedDay :
                localizedAgo + "\(day)" + " " + localizedDays
        } else {
            return localizedFromNow
        }
    }
    
    fileprivate func displayDistance(lat: String, long: String) -> String {
        
        let dbLat = Double(lat)
        let dbLong = Double(long)
        
        let stootLocation = CLLocation(latitude: dbLat!, longitude: dbLong!)
        let myLocation = CLLocation(latitude: 48.8675, longitude: 2.364) // Mock Location
        let distance = myLocation.distance(from: stootLocation) / 1000
        
        return (String(format: "à %.01f km", distance))
    }
}
