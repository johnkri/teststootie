//
//  ViewController.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import UIKit
import MapKit

protocol MainViewProtocol: class {
    func success()
    func failure(error: Error)
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: MainViewPresenterProtocol!
    
    var refreshControl: UIRefreshControl!
    
    @objc func populateData() {
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(populateData), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
}

extension MainViewController: UICollectionViewDataSource {
    
    func setupCollectionView() {
        let nib = UINib.init(nibName: "CustomCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "CustomCell")
        self.collectionView.backgroundColor? = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.list?.collection.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        cell.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        cell.layer.borderWidth = 0.4
        guard let stoot = presenter.list?.collection[indexPath.row] else { return cell }
        presenter.configure(cell: cell, stoot: stoot)
        
        return cell
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 24, left: 0, bottom: 32, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: 220)
    }
}

extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let stoot = presenter.list?.collection[indexPath.row]
        let detailViewController = ModuleBuilder.createDetailModule(stoot: stoot)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension MainViewController: MainViewProtocol {
    func success() {
        collectionView.reloadData()
    }
    
    func failure(error: Error) {
        print(error.localizedDescription)
    }
    
}

