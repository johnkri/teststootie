//
//  CustomCell.swift
//  TestMVP
//
//  Created by john kricorian on 22/12/2019.
//  Copyright © 2019 is24. All rights reserved.
//

import UIKit

protocol UserCellViewProtocol: class {
    func displayFirstname(firstname: String)
    func displayCity(city: String?)
    func displayAddress(address: String)
    func displayPackage(package: String)
    func displayUnitPrice(unitPrice: String)
    func displayImage(imageUrl: String?)
}

class CustomCell: UICollectionViewCell {
        
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var unitPriceLabel: UILabel!
    @IBOutlet weak var distanceAndcreatedDateLabel: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
    }
}

extension CustomCell: UserCellViewProtocol {

    func displayFirstname(firstname: String) {
        nameLabel.text = firstname
    }
    
    func displayCity(city: String?) {
        addressLabel.text = city
    }
    
    func displayAddress(address: String) {
        distanceAndcreatedDateLabel.text = address
    }
    
    func displayPackage(package: String) {
        packageNameLabel.text = package
    }
    
    func displayUnitPrice(unitPrice: String) {
        unitPriceLabel.text = "\(unitPrice) €"
    }
    
    func displayImage(imageUrl: String?) {
        profilePictureImageView.layer.cornerRadius = 16
        profilePictureImageView.layer.masksToBounds = true
        
        imageUrl == nil ? profilePictureImageView.image = #imageLiteral(resourceName: "default_image") : profilePictureImageView.downloaded(from: imageUrl)
    }
}
