//
//  ModuleBuilder.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation
import UIKit

protocol Builder {
    static func createMainModule() -> MainViewController
    static func createDetailModule(stoot: Stoot?) -> DetailViewController
}

class ModuleBuilder: Builder {
    
    static func createMainModule() -> MainViewController {
        
        let view = MainViewController()
        let apiClient = ApiClientImplementation(urlSessionConfiguration: URLSessionConfiguration.default, completionHandlerQueue: OperationQueue.main)
        let networkService = NetworkService(apiClient: apiClient)
        let presenter = MainViewPresenter(view: view, networkService: networkService)
        view.presenter = presenter
        
        return view
    }
    
    static func createDetailModule(stoot: Stoot?) -> DetailViewController {
        let view = DetailViewController()
        let apiClient = ApiClientImplementation(urlSessionConfiguration: URLSessionConfiguration.default, completionHandlerQueue: OperationQueue.main)
        let networkService = NetworkService(apiClient: apiClient)
        let presenter = DetailPresenter(view: view, networkService: networkService, stoot: stoot)
        view.presenter = presenter
        
        return view
    }
    
}
