//
//  DetailViewController.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var evaluationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailMapView: MKMapView!
    
    var presenter: DetailViewPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

extension DetailViewController: DetailViewProtocol {
    
    func displayPosition(position: String) {
        positionLabel.text = position
    }
    
    func displayCreatedDate(createdDate: String) {
        createdDateLabel.text = createdDate
    }
        
    func displayPrice(price: String) {
        priceLabel.text = "\(price) €"
    }
    
    func displayTitle(title: String) {
        titleLabel.text = title
    }
    
    func displayMap(region: MKCoordinateRegion) {
        detailMapView.setRegion(region, animated: false)
    }
            
    func displayEvaluation(evaluation: String) {
        evaluationLabel.text = evaluation
    }
    
    func displayImage(imageUrl: String?) {
        detailImageView.layer.cornerRadius = 16
        detailImageView.layer.masksToBounds = true
        imageUrl == nil ? detailImageView.image = #imageLiteral(resourceName: "default_image") : detailImageView.downloaded(from: imageUrl)
    }
    
    func displayFirstname(firstname: String) {
        detailLabel.text = firstname
    }
    
    func failure(error: Error) {
        print(error.localizedDescription)
    }
    
}
