//
//  DetailViewProtocol.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol DetailViewProtocol: class {
    var presenter: DetailViewPresenterProtocol! { get }
    func failure(error: Error)
    func displayFirstname(firstname: String)
    func displayEvaluation(evaluation: String)
    func displayImage(imageUrl: String?)
    func displayMap(region: MKCoordinateRegion)
    func displayTitle(title: String)
    func displayPrice(price: String)
    func displayCreatedDate(createdDate: String)
    func displayPosition(position: String)
}

protocol DetailViewPresenterProtocol: class {
    init(view: DetailViewProtocol, networkService: NetworkServiceDelegate, stoot: Stoot?)
    func configure(view: DetailViewProtocol, detail: Detail)
    func getDetail()
}

class DetailPresenter: DetailViewPresenterProtocol {
    
    var detail: Detail?
    var stoot: Stoot?
    weak var view: DetailViewProtocol?
    let networkService: NetworkServiceDelegate!
    
    required init(view: DetailViewProtocol, networkService: NetworkServiceDelegate, stoot: Stoot?) {
        self.view = view
        self.networkService = networkService
        self.stoot = stoot
        getDetail()
    }
    
     func getDetail() {
        guard let stoot = stoot else { return }
        networkService.getDetail(stootId: String(stoot.id)) { [weak self] detail in
            guard let self = self else { return }
            DispatchQueue.main.async {
                switch detail {
                case .success(let detail):
                    self.detail = detail
                    self.configure(view: self.view!, detail: self.detail!)
                case .failure(let error):
                    self.view?.failure(error: error)
                }
            }
        }
    }
    
      func configure(view: DetailViewProtocol, detail: Detail) {
        view.displayFirstname(firstname: detail.user.firstname)
        view.displayImage(imageUrl: detail.user.profilePictureUrl)
        view.displayEvaluation(evaluation: displayEvaluation(evaluation: detail.user.evalCount))
        view.displayMap(region: displayMap(detail: detail))
        view.displayTitle(title: detail.title)
        view.displayPrice(price: detail.price)
        view.displayCreatedDate(createdDate: displayCreationDate(creationDate: detail.createdAt))
        displayCity(lat: detail.lat, long: detail.lng) { city in
            self.view?.displayPosition(position: city)
        }
    }
    
        // MARK: - Private
    
    fileprivate func displayMap(detail: Detail) -> MKCoordinateRegion {
        let dbLat = Double(detail.lat)
        let dbLong = Double(detail.lng)
        let stootLocation = CLLocation(latitude: dbLat!, longitude: dbLong!)
        let regionRadius: CLLocationDistance = 100.0
        let region = MKCoordinateRegion(center: stootLocation.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        return region
    }
    
    fileprivate func displayCreationDate(creationDate: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "fr_FR_POSIX")
        
        let date = dateFormatter.date(from:creationDate)!
        
        let interval = Calendar.current.dateComponents([.year, .month, .day], from: date, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "il y à " + "\(year)" + " " + "année" : "il y à " +
                "\(year)" + " " + "années"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "il y à " +  "\(month)" + " " + "mois" : "il y à " +
                "\(month)" + " " + "mois"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "il y à " +  "\(day)" + " " + "jour" :
                "il y à " + "\(day)" + " " + "jours"
        } else {
            return "à l'instant"
        }
    }
        
    fileprivate func displayCity(lat: String, long: String, completion: @escaping (String) -> Void) {
        
        let dbLat = Double(lat)
        let dbLong = Double(long)
        let location = CLLocation(latitude: dbLat!, longitude: dbLong!)
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(location) { (location, error) in
            
            guard let location = location?.first, let city = location.subAdministrativeArea else { return }
            completion(city)
        }
    }
    
    fileprivate func displayEvaluation(evaluation: Int) -> String {
        return Int(evaluation) == 0 ? "(\(evaluation) évaluation)" : "(\(evaluation) évaluations)"
    }
}
