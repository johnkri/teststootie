//
//  Detail.swift
//  TestStootie
//
//  Created by john kricorian on 23/12/2019.
//  Copyright © 2019 john kricorian. All rights reserved.
//

import Foundation

struct Detail: Decodable {
    let id: Int
    let user: User
    let createdAt: String
    let lat: String
    let lng: String
    let title: String
    let price: String
    
    enum CodingKeys : String, CodingKey {
        case id
        case user
        case createdAt = "created_at"
        case lat
        case lng
        case title
        case price = "unit_price"
    }
    
    struct User: Decodable {
        let firstname: String
        let evalCount: Int
        let profilePictureUrl: String?

        enum CodingKeys : String, CodingKey {
            case firstname
            case evalCount = "eval_count"
            case profilePictureUrl = "profile_picture_url"
        }
    }
}
